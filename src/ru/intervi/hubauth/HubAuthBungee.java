package ru.intervi.hubauth;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.command.ConsoleCommandSender;
import net.md_5.bungee.event.EventHandler;
import ru.intervi.hubauth.socket.Request;
import ru.intervi.hubauth.socket.SocketServer;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.*;

public class HubAuthBungee extends Plugin implements Listener, SocketServer.SoListener {
    private final File PROP_FILE = new File(new File(
            this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath()
    ).getAbsoluteFile().getParentFile(), "HubAuth.properties");
    private final Set<UUID> USERS = Collections.synchronizedSet(new HashSet<>());
    private SocketServer server;
    private Thread thread;

    public HubAuthBungee() {
        if (!PROP_FILE.isFile()) {
            saveDefault();
            System.out.println("Create properties file. Edit and reload.");
        }
    }

    @Override
    public void onEnable() {
        createServer();
        getProxy().getPluginManager().registerListener(this, this);
        getProxy().getPluginManager().registerCommand(this, new ReloadCmd("hba-reload"));
    }

    @Override
    public void onDisable() {
        stopServer();
        synchronized (USERS) {
            USERS.clear();
        }
    }

    @EventHandler
    public void onLeave(PlayerDisconnectEvent event) {
        synchronized (USERS) {
            USERS.remove(event.getPlayer().getUniqueId());
        }
    }

    private void stopServer() {
        if (server != null) {
            try {
                server.stop();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (thread != null) {
            try {
                thread.interrupt();
                thread = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void createServer() {
        try {
            Map<String, String> map = load();
            server = new SocketServer(
                    map.get("host"),
                    Integer.parseInt(map.get("port")),
                    Integer.parseInt(map.get("maxconn")),
                    Integer.parseInt(map.get("timeout")),
                    this
            );
            server.start();
            thread = new Thread(() -> server.work());
            thread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Map<String, String> load() {
        HashMap<String, String> result = new HashMap<>();
        try {
            Properties prop = new Properties();
            prop.load(new FileReader(PROP_FILE));
            for (String key : prop.stringPropertyNames()) {
                result.put(key, prop.getProperty(key));
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private void save(Map<String, String> map) {
        try {
            Properties prop = new Properties();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                prop.setProperty(entry.getKey(), entry.getValue());
            }
            prop.store(new FileWriter(PROP_FILE), "socket server settings");
        }
        catch(Exception e) {e.printStackTrace();}
    }

    private void saveDefault() {
        HashMap<String, String> map = new HashMap<>();
        map.put("host", "127.0.0.1");
        map.put("port", "25520");
        map.put("maxconn", "100");
        map.put("timeout", "500");
        save(map);
    }

    @Override
    public String request(String data) {
        try {
            Map.Entry<String, String> entry = Request.parseRequest(data);
            switch (entry.getKey()) {
                case "auth":
                    synchronized (USERS) {
                        USERS.add(UUID.fromString(entry.getValue()));
                    }
                    return null;
                case "isauth":
                    synchronized (USERS) {
                        return String.valueOf(USERS.contains(UUID.fromString(entry.getValue())));
                    }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private class ReloadCmd extends Command {
        ReloadCmd(String name) {
            super(name);
        }

        @SuppressWarnings("deprecation")
        @Override
        public void execute(CommandSender commandSender, String[] strings) {
            if (!(commandSender instanceof ConsoleCommandSender)) {
                commandSender.sendMessage("only console");
                return;
            }
            stopServer();
            createServer();
            commandSender.sendMessage("prop reloaded and socket server recreated");
        }
    }
}
