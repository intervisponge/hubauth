package ru.intervi.hubauth;

import com.flowpowered.math.vector.Vector3d;
import com.flowpowered.math.vector.Vector3i;
import com.google.common.reflect.TypeToken;
import com.google.inject.Inject;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandMapping;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.args.GenericArguments;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.ConfigDir;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.command.SendCommandEvent;
import org.spongepowered.api.event.command.TabCompleteEvent;
import org.spongepowered.api.event.entity.MoveEntityEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartingServerEvent;
import org.spongepowered.api.event.game.state.GameStoppedEvent;
import org.spongepowered.api.event.network.ClientConnectionEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.plugin.PluginContainer;
import org.spongepowered.api.profile.property.ProfileProperty;
import org.spongepowered.api.scheduler.Task;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.storage.WorldProperties;
import ru.intervi.hubauth.socket.Request;
import ru.intervi.hubauth.socket.SocketClient;

import javax.annotation.Nonnull;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@Plugin(
        id = "hubauth", name = "HubAuth", version = "1.0",
        authors = {"InterVi"},
        description = "Auth for hub server, bungee support."
)
public class HubAuthSponge {
    @Inject
    @ConfigDir(sharedRoot = false)
    private Path configDir;

    private Path configPath;
    private Path dbPath;
    private ConfigurationLoader<CommentedConfigurationNode> configManager;
    private CommentedConfigurationNode config;
    private Vector3d spawnPos, afterPos, spawnRotate, afterRotate;
    private UUID spawnId, afterId;
    private Task task;
    private SocketClient socketClient;

    private H2DataBase db;
    private Map<UUID, Long> USERS = Collections.synchronizedMap(new ConcurrentHashMap<>());

    private void setupConfig() {
        configPath = Paths.get(configDir.toString(), "config.conf");
        configManager = HoconConfigurationLoader.builder().setPath(configPath).build();
        try {
            if (!Files.exists(configDir)) {
                Files.createDirectories(configDir);
            }
            if (!Files.exists(configPath)) {
                Files.createFile(configPath);
                config = configManager.createEmptyNode();
                config.getNode("translate", "registered").setValue("you success register");
                config.getNode("translate", "passwd-noequal").setValue("passwords do not match");
                config.getNode("translate", "plugin-error").setValue("Server error. Please report admin.");
                config.getNode("translate", "user-not-found").setValue("you not register");
                config.getNode("translate", "authed").setValue("You success auth. Prev ip: %s");
                config.getNode("translate", "bad-passwd").setValue("wrong password");
                config.getNode("translate", "need-auth").setValue("need auth first");
                config.getNode("translate", "bad-old-passwd").setValue("wrong old password");
                config.getNode("translate", "passwd-changed").setValue("password success changed");
                config.getNode("translate", "name-case").setValue("please, join it %s");
                config.getNode("translate", "bad-uuid").setValue("incorrect uuid, need %s");
                config.getNode("translate", "unregistered").setValue("you success unregistered");
                config.getNode("translate", "alert-auth").setValue("use /a password");
                config.getNode("translate", "alert-reg").setValue("use /reg password password");
                config.getNode("translate", "already-reg").setValue("you already registered");
                config.getNode("translate", "already-auth").setValue("you already auth");
                config.getNode("translate", "long-auth").setValue("long auth");
                config.getNode("translate", "deny-nick").setValue("%s nickname is deny");
                config.getNode("translate", "long-nick").setValue("you nickname is long");
                config.getNode("translate", "sync-error").setValue("Sync error. Attempt now or rejoin.");
                config.getNode("hash", "algorithm").setValue("SHA-256");
                config.getNode("hash", "salt").setValue("sdlkfhisunsljdfhj");
                config.getNode("hash", "repeat").setValue(2);
                config.getNode("prevent", "kick-timer").setValue(60);
                config.getNode("prevent", "move").setValue(true);
                config.getNode("prevent", "commands").setValue(true);
                config.getNode("nicks", "filter").setValue(true);
                config.getNode("nicks", "regexp").setValue("[a-zA-Z0-9_]*");
                config.getNode("nicks", "length").setValue(16);
                config.getNode("nicks", "deny-list").setValue(Collections.singletonList("(admin)*"));
                config.getNode("points", "enabled").setValue(false);
                config.getNode("socket", "enabled").setValue(false);
                config.getNode("socket", "host").setValue("127.0.0.1");
                config.getNode("socket", "port").setValue(25520);
                config.getNode("socket", "timeout").setValue(2000);
                WorldProperties world = Sponge.getServer().getDefaultWorld().get();
                Vector3i worldSpawn = world.getSpawnPosition();
                Vector3d spawnPoint = new Vector3d(worldSpawn.getX(), worldSpawn.getY(), worldSpawn.getZ());
                spawnPos = afterPos = spawnPoint;
                spawnId = afterId = world.getUniqueId();
                spawnRotate = afterRotate = new Vector3d(0, 0, 0);
                savePoints();
                configManager.save(config);
            } else {
                config = configManager.load();
                double sx = config.getNode("points", "spawn", "x").getDouble();
                double sy = config.getNode("points", "spawn", "y").getDouble();
                double sz = config.getNode("points", "spawn", "z").getDouble();
                String sn = config.getNode("points", "spawn", "world").getString();
                spawnPos = new Vector3d(sx, sy, sz);
                spawnId = Sponge.getServer().getWorld(sn).get().getUniqueId();
                double srx = config.getNode("points", "spawn", "rx").getDouble();
                double sry = config.getNode("points", "spawn", "ry").getDouble();
                double srz = config.getNode("points", "spawn", "rz").getDouble();
                spawnRotate = new Vector3d(srx, sry, srz);
                double ax = config.getNode("points", "after", "x").getDouble();
                double ay = config.getNode("points", "after", "y").getDouble();
                double az = config.getNode("points", "after", "z").getDouble();
                String an = config.getNode("points", "after", "world").getString();
                afterPos = new Vector3d(ax, ay, az);
                afterId = Sponge.getServer().getWorld(an).get().getUniqueId();
                double arx = config.getNode("points", "after", "rx").getDouble();
                double ary = config.getNode("points", "after", "ry").getDouble();
                double arz = config.getNode("points", "after", "rz").getDouble();
                afterRotate = new Vector3d(arx, ary, arz);
                createSocket();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void savePoints() {
        config.getNode("points", "spawn", "x").setValue(spawnPos.getX());
        config.getNode("points", "spawn", "y").setValue(spawnPos.getY());
        config.getNode("points", "spawn", "z").setValue(spawnPos.getZ());
        config.getNode("points", "spawn", "rx").setValue(spawnRotate.getX());
        config.getNode("points", "spawn", "ry").setValue(spawnRotate.getY());
        config.getNode("points", "spawn", "rz").setValue(spawnRotate.getZ());
        config.getNode("points", "spawn", "world").setValue(Sponge.getServer().getWorld(spawnId).get().getName());
        config.getNode("points", "after", "x").setValue(afterPos.getX());
        config.getNode("points", "after", "y").setValue(afterPos.getY());
        config.getNode("points", "after", "z").setValue(afterPos.getZ());
        config.getNode("points", "after", "rx").setValue(spawnRotate.getX());
        config.getNode("points", "after", "ry").setValue(spawnRotate.getY());
        config.getNode("points", "after", "rz").setValue(spawnRotate.getZ());
        config.getNode("points", "after", "world").setValue(Sponge.getServer().getWorld(afterId).get().getName());
    }

    @Listener
    public void onStarting(GameStartingServerEvent event) {
        setupConfig();
        dbPath = Paths.get(configDir.toString(), "users");
        try {
            db = new H2DataBase(dbPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Listener
    public void onPreInit(GamePreInitializationEvent event) {
        CommandSpec reload = CommandSpec.builder()
                .description(Text.of("Config reload command."))
                .permission("hubauth.cmd.admin.reload")
                .executor((@Nonnull CommandSource src, @Nonnull CommandContext args) -> {
                    if (!checkPlayerAuth(src)) return CommandResult.success();
                    setupConfig();
                    createSocket();
                    src.sendMessage(Text.of("config reloaded"));
                    return CommandResult.success();
                })
                .build();
        CommandSpec userinfo = CommandSpec.builder()
                .description(Text.of("Show user information."))
                .permission("hubauth.cmd.admin.userinfo")
                .arguments(GenericArguments.string(Text.of("name")))
                .executor((@Nonnull CommandSource src, @Nonnull CommandContext args) -> {
                    if (!checkPlayerAuth(src)) return CommandResult.success();
                    String name = args.<String>getOne("name").get();
                    try {
                        Map<String, Object> user = db.getUser(name);
                        if (user.isEmpty()) {
                            src.sendMessage(Text.of(name + " not found"));
                        } else {
                            Date regDate = new Date((Long) user.get("reg"));
                            Date lastDate = new Date((Long) user.get("last"));
                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String info = String.format(
                                    "UUID: %s\nName: %s\nReg: %s\nLast: %s",
                                    user.get("uuid"), user.get("name"),
                                    format.format(regDate), format.format(lastDate)
                            );
                            src.sendMessage(Text.of(info));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        src.sendMessage(Text.of("error, see console"));
                    }
                    return CommandResult.success();
                })
                .build();
        CommandSpec remove = CommandSpec.builder()
                .description(Text.of("Remove user from auth system."))
                .permission("hubauth.cmd.admin.remove")
                .arguments(GenericArguments.string(Text.of("name")))
                .executor((@Nonnull CommandSource src, @Nonnull CommandContext args) -> {
                    if (!checkPlayerAuth(src)) return CommandResult.success();
                    String name = args.<String>getOne("name").get();
                    try {
                        db.removeUser(name);
                        src.sendMessage(Text.of(name + " removed"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        src.sendMessage(Text.of("error, see console"));
                    }
                    return CommandResult.success();
                })
                .build();
        CommandSpec clear = CommandSpec.builder()
                .description(Text.of("Clear users from auth system."))
                .permission("hubauth.cmd.admin.clear")
                .arguments(GenericArguments.longNum(Text.of("time")))
                .executor((@Nonnull CommandSource src, @Nonnull CommandContext args) -> {
                    if (!checkPlayerAuth(src)) return CommandResult.success();
                    long time = args.<Long>getOne("time").get();
                    try {
                        int result = db.clear(time);
                        src.sendMessage(Text.of("cleared " + result + " accounts"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        src.sendMessage(Text.of("error, see console"));
                    }
                    return CommandResult.success();
                })
                .build();
        CommandSpec adminChPasswd = CommandSpec.builder()
                .description(Text.of("Change user password auth system."))
                .permission("hubauth.cmd.admin.chpasswd")
                .arguments(
                        GenericArguments.string(Text.of("name")),
                        GenericArguments.string(Text.of("passwd"))
                )
                .executor((@Nonnull CommandSource src, @Nonnull CommandContext args) -> {
                    if (!checkPlayerAuth(src)) return CommandResult.success();
                    String name = args.<String>getOne("name").get();
                    String passwd = args.<String>getOne("passwd").get();
                    try {
                        db.changeUser(name, Collections.singletonMap(name, hashPassword(passwd)));
                        src.sendMessage(Text.of(name + " changed password to " + passwd));
                    } catch (Exception e) {
                        e.printStackTrace();
                        src.sendMessage(Text.of("error, see console"));
                    }
                    return CommandResult.success();
                })
                .build();
        CommandSpec size = CommandSpec.builder()
                .description(Text.of("Show count users in auth system."))
                .permission("hubauth.cmd.admin.size")
                .executor((@Nonnull CommandSource src, @Nonnull CommandContext args) -> {
                    try {
                        if (!checkPlayerAuth(src)) return CommandResult.success();
                        int dbsize = db.size();
                        if (dbsize == -1) {
                            src.sendMessage(Text.of("sql error"));
                        } else {
                            src.sendMessage(Text.of("Users count: " + dbsize));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        src.sendMessage(Text.of("error, see console"));
                    }
                    return CommandResult.success();
                })
                .build();
        CommandSpec setSpawn = CommandSpec.builder()
                .description(Text.of("Set unauthed spawn point."))
                .permission("hubauth.cmd.admin.setspawn")
                .executor((@Nonnull CommandSource src, @Nonnull CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(Text.of("only in game"));
                        return CommandResult.success();
                    }
                    if (!checkPlayerAuth(src)) return CommandResult.success();
                    Player player = (Player) src;
                    spawnPos = player.getPosition();
                    spawnId = player.getWorldUniqueId().get();
                    spawnRotate = player.getRotation();
                    try {
                        savePoints();
                        configManager.save(config);
                        src.sendMessage(Text.of("first spawn point set"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        src.sendMessage(Text.of("error, see console"));
                    }
                    return CommandResult.success();
                })
                .build();
        CommandSpec setTeleport = CommandSpec.builder()
                .description(Text.of("Set point for after auth teleport."))
                .permission("hubauth.cmd.admin.settp")
                .executor((@Nonnull CommandSource src, @Nonnull CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(Text.of("only in game"));
                        return CommandResult.success();
                    }
                    if (!checkPlayerAuth(src)) return CommandResult.success();
                    Player player = (Player) src;
                    afterPos = player.getPosition();
                    afterId = player.getWorldUniqueId().get();
                    afterRotate = player.getRotation();
                    try {
                        savePoints();
                        configManager.save(config);
                        src.sendMessage(Text.of("after tp point set"));
                    } catch (Exception e) {
                        e.printStackTrace();
                        src.sendMessage(Text.of("error, see console"));
                    }
                    return CommandResult.success();
                })
                .build();
        CommandSpec register = CommandSpec.builder()
                .description(Text.of("Register in auth system."))
                .permission("hubauth.cmd.user.register")
                .arguments(
                        GenericArguments.string(Text.of("passwd")),
                        GenericArguments.string(Text.of("passwd2"))
                )
                .executor((@Nonnull CommandSource src, @Nonnull CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(Text.of("only in game"));
                        return CommandResult.success();
                    }
                    String passwd = args.<String>getOne("passwd").get();
                    String passwd2 = args.<String>getOne("passwd2").get();
                    Player player = (Player) src;
                    try {
                        synchronized (USERS) {
                            if (!USERS.containsKey(player.getUniqueId())) {
                                src.sendMessage(Text.of(config.getNode("translate", "sync-error").getString()));
                                return CommandResult.success();
                            }
                            if (USERS.get(player.getUniqueId()) == 0) {
                                src.sendMessage(Text.of(config.getNode("translate", "already-auth").getString()));
                                return CommandResult.success();
                            }
                        }
                        if (!db.getUser(player.getName()).isEmpty()) {
                            src.sendMessage(Text.of(config.getNode("translate", "already-reg").getString()));
                            return CommandResult.success();
                        }
                        if (passwd.equals(passwd2)) {
                            db.addUser(
                                    player.getUniqueId(), player.getName(), hashPassword(passwd),
                                    player.getConnection().getAddress().getAddress().getHostAddress()
                            );
                            setAuth(player.getUniqueId());
                            synchronized (USERS) {
                                USERS.put(player.getUniqueId(), 0l);
                            }
                            if (config.getNode("points", "enabled").getBoolean()) {
                                player.setLocation(
                                        new Location<>(Sponge.getServer().getWorld(afterId).get(), afterPos)
                                );
                                player.setRotation(afterRotate);
                            }
                            src.sendMessage(Text.of(config.getNode("translate", "registered").getString()));
                        } else {
                            src.sendMessage(Text.of(config.getNode("translate", "passwd-noequal").getString()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        src.sendMessage(Text.of(config.getNode("translate", "plugin-error").getString()));
                    }
                    return CommandResult.success();
                })
                .build();
        CommandSpec auth = CommandSpec.builder()
                .description(Text.of("Auth in auth system."))
                .permission("hubauth.cmd.user.auth")
                .arguments(GenericArguments.string(Text.of("passwd")))
                .executor((@Nonnull CommandSource src, @Nonnull CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(Text.of("only in game"));
                        return CommandResult.success();
                    }
                    Player player = (Player) src;
                    synchronized (USERS) {
                        if (!USERS.containsKey(player.getUniqueId())) {
                            src.sendMessage(Text.of(config.getNode("translate", "sync-error").getString()));
                            return CommandResult.success();
                        }
                        if (USERS.get(player.getUniqueId()) == 0) {
                            src.sendMessage(Text.of(config.getNode("translate", "already-auth").getString()));
                            return CommandResult.success();
                        }
                    }
                    String passwd = args.<String>getOne("passwd").get();
                    try {
                        Map<String, Object> user = db.getUser(src.getName());
                        if (user.isEmpty()) {
                            src.sendMessage(Text.of(config.getNode("translate", "user-not-found").getString()));
                        } else if (hashPassword(passwd).equals((String) user.get("password"))) {
                            setAuth(player.getUniqueId());
                            synchronized (USERS) {
                                USERS.put(player.getUniqueId(), 0l);
                            }
                            if (config.getNode("points", "enabled").getBoolean()) {
                                player.setLocation(
                                        new Location<>(Sponge.getServer().getWorld(afterId).get(), afterPos)
                                );
                                player.setRotation(afterRotate);
                            }
                            db.updateLast(player.getName(), player.getConnection().getAddress().getAddress().getHostAddress());
                            src.sendMessage(Text.of(
                                    String.format(config.getNode("translate", "authed").getString(),
                                    (String) user.get("ip"))
                            ));
                        } else {
                            src.sendMessage(Text.of(config.getNode("translate", "bad-passwd").getString()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        src.sendMessage(Text.of(config.getNode("translate", "plugin-error").getString()));
                    }
                    return CommandResult.success();
                })
                .build();
        CommandSpec chPasswd = CommandSpec.builder()
                .description(Text.of("Change password in auth system."))
                .permission("hubauth.cmd.user.chpasswd")
                .arguments(
                        GenericArguments.string(Text.of("oldpasswd")),
                        GenericArguments.string(Text.of("passwd")),
                        GenericArguments.string(Text.of("passwd2"))
                )
                .executor((@Nonnull CommandSource src, @Nonnull CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(Text.of("only in game"));
                        return CommandResult.success();
                    }
                    if (!checkPlayerAuth(src)) return CommandResult.success();
                    Player player = (Player) src;
                    String oldpasswd = args.<String>getOne("oldpasswd").get();
                    String passwd = args.<String>getOne("passwd").get();
                    String passwd2 = args.<String>getOne("passwd2").get();
                    try {
                        if (!db.passwordEquals(player.getName(), hashPassword(oldpasswd))) {
                            src.sendMessage(Text.of(config.getNode("translate", "bad-old-passwd").getString()));
                            return CommandResult.success();
                        }
                        if (passwd.equals(passwd2)) {
                            db.changeUser(
                                    player.getName(),
                                    Collections.singletonMap("password", hashPassword(passwd))
                            );
                            src.sendMessage(Text.of(config.getNode("translate", "passwd-changed").getString()));
                        } else {
                            src.sendMessage(Text.of(config.getNode("translate", "passwd-noequal").getString()));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        src.sendMessage(Text.of(config.getNode("translate", "plugin-error").getString()));
                    }
                    return CommandResult.success();
                })
                .build();
        CommandSpec unregister = CommandSpec.builder()
                .description(Text.of("Remove you from auth system."))
                .permission("hubauth.cmd.user.unregister")
                .executor((@Nonnull CommandSource src, @Nonnull CommandContext args) -> {
                    if (!(src instanceof Player)) {
                        src.sendMessage(Text.of("only in game"));
                        return CommandResult.success();
                    }
                    if (!checkPlayerAuth(src)) return CommandResult.success();
                    Player player = (Player) src;
                    try {
                        db.removeUser(player.getName());
                        src.sendMessage(Text.of(config.getNode("translate", "unregistered").getString()));
                    } catch (Exception e) {
                        e.printStackTrace();
                        src.sendMessage(Text.of("error, see console"));
                    }
                    return CommandResult.success();
                })
                .build();
        Sponge.getCommandManager().register(this, reload, "hba-reload", "hubauth-reload");
        Sponge.getCommandManager().register(this, userinfo, "hba-userinfo", "hubauth-userinfo");
        Sponge.getCommandManager().register(this, remove, "hba-remove", "hubauth-remove");
        Sponge.getCommandManager().register(this, clear, "hba-clear", "hubauth-clear");
        Sponge.getCommandManager().register(this, adminChPasswd, "hba-chpass", "hubauth-chpass");
        Sponge.getCommandManager().register(this, size, "hba-size", "hubauth-size");
        Sponge.getCommandManager().register(this, setSpawn, "hba-setspawn", "hubauth-setspawn");
        Sponge.getCommandManager().register(this, setTeleport, "hba-settp", "hubauth-settp");
        Sponge.getCommandManager().register(this, register, "reg", "register", "hba-reg");
        Sponge.getCommandManager().register(this, auth, "l", "login", "a", "auth", "hba-auth");
        Sponge.getCommandManager().register(this, chPasswd, "changepassword", "chpass");
        Sponge.getCommandManager().register(this, unregister, "unreg", "unregister", "hba-unreg");
        task = Sponge.getScheduler().createTaskBuilder()
                .execute(
                        () -> {
                            try {
                                long time = System.currentTimeMillis();
                                int sec = config.getNode("prevent", "kick-timer").getInt() * 1000;
                                for (Player player : Sponge.getServer().getOnlinePlayers()) {
                                    synchronized (USERS) {
                                        if (!USERS.containsKey(player.getUniqueId())) continue;
                                        long join = USERS.get(player.getUniqueId());
                                        if (join == 0) continue;
                                        if (time - join >= sec) {
                                            player.kick(Text.of(config.getNode("translate", "long-auth").getString()));
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                )
                .interval(1, TimeUnit.SECONDS)
                .name("HubAuthKicker").submit(this);
    }

    private boolean checkPlayerAuth(CommandSource src) {
        synchronized (USERS) {
            if (src instanceof Player) {
                Player player = (Player) src;
                if (!USERS.containsKey(player.getUniqueId())) {
                    src.sendMessage(Text.of(config.getNode("translate", "sync-error").getString()));
                    return false;
                }
                if (USERS.get(player.getUniqueId()) > 0) {
                    src.sendMessage(Text.of(config.getNode("translate", "need-auth").getString()));
                    return false;
                }
            }
            return true;
        }
    }

    @Listener
    public void onStopped(GameStoppedEvent event) {
        if (task != null) task.cancel();
    }

    private void createSocket() {
        if (config.getNode("socket", "enabled").getBoolean()) {
            socketClient = new SocketClient(
                    config.getNode("socket", "host").getString(),
                    config.getNode("socket", "port").getInt(),
                    config.getNode("socket", "timeout").getInt()
            );
        } else {
            socketClient = null;
        }
    }

    private String hashPassword(String raw) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance(config.getNode("hash", "algorithm").getString());
        String result = raw + config.getNode("hash", "salt").getString();
        for (int i = 0; i < config.getNode("hash", "repeat").getInt(); i++) {
            result = Base64.getEncoder().encodeToString(digest.digest(result.getBytes(StandardCharsets.UTF_8)));
        }
        return result;
    }

    private void setAuth(UUID uuid) {
        if (socketClient == null) return;
        try {
            socketClient.request(Request.requestSetAuth(uuid));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isAuth(UUID uuid) {
        if (socketClient == null) return false;
        try {
            return Request.parseAuthReply(socketClient.request(Request.requestIsAuth(uuid)));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean isMyCommand(String command) {
        Optional<? extends CommandMapping> cmd = Sponge.getCommandManager().get(command);
        if (!cmd.isPresent()) return false;
        Optional<? extends PluginContainer> plugin = Sponge.getCommandManager().getOwner(cmd.get());
        return plugin.map(pluginContainer -> pluginContainer.getName().equals("HubAuth")).orElse(false);
    }

    private boolean isAllowedCommand(UUID uuid, String command) {
        if (!config.getNode("prevent", "commands").getBoolean()) return true;
        boolean deny;
        synchronized (USERS) {
            deny = !USERS.containsKey(uuid) || USERS.get(uuid) > 0;
        }
        return !deny || isMyCommand(command);
    }

    @Listener
    public void onMove(MoveEntityEvent event) {
        if (event instanceof MoveEntityEvent.Teleport) return;
        UUID uuid = event.getTargetEntity().getUniqueId();
        boolean deny;
        synchronized (USERS) {
            deny = !USERS.containsKey(uuid) || USERS.get(uuid) > 0;
        }
        if (
                event.getCause().first(Player.class).isPresent()
                && deny
                && config.getNode("prevent", "move").getBoolean()
        ) {
            event.setCancelled(true);
        }
    }

    @Listener
    public void onLeave(ClientConnectionEvent.Disconnect event) {
        synchronized (USERS) {
            USERS.remove(event.getTargetEntity().getUniqueId());
        }
    }

    @Listener
    public void onCmd(SendCommandEvent event) {
        if (!(event.getSource() instanceof Player)) return;
        UUID uuid = ((Player) event.getSource()).getUniqueId();
        if (!isAllowedCommand(uuid, event.getCommand())) {
            event.setCancelled(true);
        }
    }

    @Listener
    public void onTab(TabCompleteEvent event) {
        if (!(event.getSource() instanceof Player)) return;
        int ind = event.getRawMessage().indexOf(' ');
        if (ind == -1) return;
        String command = event.getRawMessage().substring(0, ind);
        if (!(event.getSource() instanceof Player)) return;
        UUID uuid = ((Player) event.getSource()).getUniqueId();
        if (!isAllowedCommand(uuid, command)) {
            event.setCancelled(true);
        }
    }

    @Listener
    public void onAuth(ClientConnectionEvent.Auth event) {
        try {
            String name = event.getProfile().getName().get();
            if (config.getNode("nicks", "filter").getBoolean()) {
                if (name.length() > config.getNode("nicks", "length").getInt()) {
                    event.setMessage(Text.of(config.getNode("translate", "long-nick").getString()));
                    event.setCancelled(true);
                    return;
                } else if (!name.matches(config.getNode("nicks", "regexp").getString())) {
                    event.setMessage(Text.of(String.format(config.getNode("translate", "deny-nick").getString(), name)));
                    event.setCancelled(true);
                    return;
                } else {
                    for (String regexp : config.getNode("nicks", "deny-list").getList(TypeToken.of(String.class))) {
                        if (name.matches(regexp)) {
                            event.setMessage(Text.of(config.getNode("translate", "long-nick").getString()));
                            event.setCancelled(true);
                            return;
                        }
                    }
                }
            }
            UUID uuid = event.getProfile().getUniqueId();
            if (isAuth(uuid)) {
                synchronized (USERS) {
                    USERS.put(uuid, 0l);
                }
                return;
            }
            Map<String, Object> user = db.getUser(name);
            if (user.isEmpty()) return;
            if (!name.equals((String) user.get("name"))) {
                event.setMessage(
                        Text.of(String.format(config.getNode("translate", "name-case").getString(), (String) user.get("name")))
                );
                event.setCancelled(true);
                return;
            }
            if (!uuid.equals((UUID) user.get("uuid"))) {
                event.setMessage(
                        Text.of(String.format(config.getNode("translate", "bad-uuid").getString(), user.get("uuid").toString()))
                );
                event.setCancelled(true);
                return;
            }
            event.getProfile().addProperty("found", ProfileProperty.of("found", "true"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Listener
    public void onJoin(ClientConnectionEvent.Join event) {
        Player player = event.getTargetEntity();
        synchronized (USERS) {
            if (USERS.containsKey(player.getUniqueId()) && USERS.get(player.getUniqueId()) == 0) return;
        }
        if (player.getProfile().getPropertyMap().containsKey("found")) {
            event.getTargetEntity().sendMessage(Text.of(config.getNode("translate", "alert-auth").getString()));
        } else {
            event.getTargetEntity().sendMessage(Text.of(config.getNode("translate", "alert-reg").getString()));
        }
        synchronized (USERS) {
            USERS.put(player.getUniqueId(), System.currentTimeMillis());
        }
        if (config.getNode("points", "enabled").getBoolean()) {
            player.setLocation(
                    new Location<>(Sponge.getServer().getWorld(spawnId).get(), spawnPos)
            );
            player.setRotation(spawnRotate);
        }
    }
}
