package ru.intervi.hubauth.socket;

import java.util.AbstractMap;
import java.util.Map;
import java.util.UUID;

public class Request {
    public static String requestSetAuth(UUID uuid) {
        return "auth=" + uuid.toString();
    }

    public static String requestIsAuth(UUID uuid) {
        return "isauth=" + uuid.toString();
    }

    public static boolean parseAuthReply(String reply) {
        return Boolean.valueOf(reply);
    }

    public static Map.Entry<String, String> parseRequest(String request) {
        String[] result = request.split("=");
        return new AbstractMap.SimpleEntry<>(result[0].trim(), result[1].trim());
    }
}
