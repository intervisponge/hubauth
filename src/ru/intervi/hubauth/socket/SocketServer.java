package ru.intervi.hubauth.socket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;

public class SocketServer {
    private final String HOST;
    private final int PORT, MAXCONN, TIMEOUT;
    private final SoListener LISTENER;
    private volatile ServerSocket server;
    private boolean stop = false;

    public SocketServer(String host, int port, int maxconn, int timeout, SoListener listener) {
        HOST = host;
        PORT = port;
        MAXCONN = maxconn;
        TIMEOUT = timeout;
        LISTENER = listener;
    }

    public interface SoListener {
        String request(String data);
    }

    public void start() throws IOException {
        server = new ServerSocket(PORT, MAXCONN, InetAddress.getByName(HOST));
        stop = false;
    }

    public void stop() throws IOException {
        if (server != null) server.close();
        stop = true;
    }

    public void work() {
        while (true) {
            try {
                Socket socket = server.accept();
                try {
                    socket.setSoTimeout(TIMEOUT);
                    InputStream input = socket.getInputStream();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buff = new byte[128];
                    while (input.read(buff) > 0) {
                        baos.write(buff);
                    }
                    String result = new String(baos.toByteArray());
                    String reply = LISTENER.request(result);
                    if (reply == null || reply.isEmpty()) reply = "null";
                    socket.getOutputStream().write(reply.getBytes(Charset.forName("UTF-8")));
                    socket.getOutputStream().flush();
                    socket.shutdownOutput();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                if (stop) break;
                e.printStackTrace();
                try {
                    server.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                try {
                    start();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
