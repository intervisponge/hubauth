package ru.intervi.hubauth.socket;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.nio.charset.Charset;

public class SocketClient {
    private final String HOST;
    private final int PORT;
    private final int TIMEOUT;

    public SocketClient(String host, int port, int timeout) {
        HOST = host;
        PORT = port;
        TIMEOUT = timeout;
    }

    public String request(String data) throws IOException {
        Socket socket = null;
        try {
            socket = new Socket(HOST, PORT);
            socket.setSoTimeout(TIMEOUT);
            socket.getOutputStream().write(data.getBytes(Charset.forName("UTF-8")));
            socket.getOutputStream().flush();
            socket.shutdownOutput(); //if else not sent bug
            InputStream input = socket.getInputStream();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buff = new byte[128];
            while (input.read(buff) > 0) {
                baos.write(buff);
            }
            return new String(baos.toByteArray()).trim();
        } finally {
            if (socket != null) socket.close();
        }
    }
}
