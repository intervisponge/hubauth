package ru.intervi.hubauth;

import org.spongepowered.api.Sponge;
import org.spongepowered.api.service.sql.SqlService;

import java.nio.file.Path;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class H2DataBase {
    private final String URI;
    private final SqlService SERVICE;

    public H2DataBase(Path path) throws SQLException {
        URI = "jdbc:h2:" + path.toString();
        SERVICE = Sponge.getServiceManager().provide(SqlService.class).get();
        createTable();
    }

    private boolean execSql(String sql) throws SQLException {
        Connection conn = SERVICE.getDataSource(URI).getConnection();
        try {
            boolean result = conn.prepareStatement(sql).execute();
            conn.commit();
            return result;
        } finally {
            conn.close();
        }
    }

    private void createTable() throws SQLException {
        String sql = "CREATE TABLE IF NOT EXISTS users(" +
                "uuid UUID not null, " +
                "name VARCHAR not null, " +
                "password VARCHAR not null, " +
                "reg BIGINT not null, " +
                "last BIGINT not null, " +
                "ip VARCHAR not null" +
                ");";
        execSql(sql);
    }

    public void addUser(UUID uuid, String name, String password, String ip) throws SQLException {
        long time = System.currentTimeMillis();
        String sql = "INSERT INTO users(`uuid`, `name`, `password`, `reg`, `last`, `ip`) VALUES('"
                + uuid.toString() + "', '" + name + "', '" + password + "', '" + time + "', '" + time +
                "', '" + ip + "');";
        execSql(sql);
    }

    public Map<String, Object> getUser(String name) throws SQLException {
        String sql = "SELECT * FROM users WHERE lower(`name`)='" + name.toLowerCase() + "';";
        HashMap<String, Object> result = new HashMap<>();
        Connection conn = SERVICE.getDataSource(URI).getConnection();
        try {
            ResultSet set = conn.prepareStatement(sql).executeQuery();
            if (set.first()) {
                result.put("uuid", set.getObject(set.findColumn("uuid")));
                result.put("name", set.getString(set.findColumn("name")));
                result.put("password", set.getString(set.findColumn("password")));
                result.put("reg", set.getLong(set.findColumn("reg")));
                result.put("last", set.getLong(set.findColumn("last")));
                result.put("ip", set.getString(set.findColumn("ip")));
            }
            return result;
        } finally {
            conn.close();
        }
    }

    public boolean passwordEquals(String name, String password) throws SQLException {
        String sql = "SELECT `password` FROM users WHERE lower(`name`)='" + name.toLowerCase() + "';";
        Connection conn = SERVICE.getDataSource(URI).getConnection();
        try {
            ResultSet set = conn.prepareStatement(sql).executeQuery();
            if (!set.first()) return false;
            return set.getString(set.findColumn("password")).equals(password);
        } finally {
            conn.close();
        }
    }

    public void removeUser(String name) throws SQLException {
        String sql = "DELETE from users WHERE lower(`name`)='" + name.toLowerCase() + "';";
        execSql(sql);
    }

    public void changeUser(String name, Map<String, Object> map) throws SQLException {
        String set = "";
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            set += '`' + entry.getKey() + "`='" + entry.getValue().toString() + "', ";
        }
        set = set.substring(0, set.length() - 2);
        String sql = "UPDATE users SET " + set + " WHERE lower(`name`)='" + name.toLowerCase() + "';";
        execSql(sql);
    }

    public void updateLast(String name, String ip) throws SQLException {
        long time = System.currentTimeMillis();
        String sql = "UPDATE users SET `last`='" + time + "', `ip`='" + ip +
                "' WHERE lower(`name`)='" + name.toLowerCase() + "';";
        execSql(sql);
    }

    public int clear(long time) throws SQLException {
        String sql = "DELETE from users WHERE `last`<='" + time + "';";
        Connection conn = SERVICE.getDataSource(URI).getConnection();
        try {
            int result = conn.prepareStatement(sql).executeUpdate();
            conn.commit();
            return result;
        } finally {
            conn.close();
        }
    }

    public int size() throws SQLException {
        String sql = "SELECT COUNT(*) FROM `users`;";
        Connection conn = SERVICE.getDataSource(URI).getConnection();
        try {
            ResultSet set = conn.prepareStatement(sql).executeQuery();
            if (!set.first()) return -1;
            return set.getInt(0);
        } finally {
            conn.close();
        }
    }
}
